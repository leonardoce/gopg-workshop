package main

import (
	"fmt"
	"sync"
)

func main() {
	var wg sync.WaitGroup
	var ch = make(chan int, 10)

	wg.Add(10)

	for i := 0; i < 10; i++ {
		ch <- i
	}

	for i := 0; i < 10; i++ {
		go func() {
			defer wg.Done()
			fmt.Println("Numero", <-ch)
		}()
	}

	wg.Wait()
}
