package main

import (
	"database/sql"
	"log"
	"os"
	"strconv"
	"sync"
	"time"

	_ "github.com/lib/pq"
)

const totalRuns = 10000

func main() {
	if len(os.Args) <= 1 {
		log.Fatal("Usage: ", os.Args[0], " <nthreads>")
	}

	concurrentThreads, err := strconv.Atoi(os.Args[1])
	if err != nil {
		log.Fatal("Usage: ", os.Args[0], " <nthreads>")
	}

	db, err := sql.Open("postgres", "dbname=postgres user=postgres host=127.0.0.1 sslmode=disable")
	if err != nil {
		log.Fatal("sql.Open failed", err)
	}

	err = db.Ping()
	if err != nil {
		log.Fatal("db.Ping", err)
	}

	var wg sync.WaitGroup
	wg.Add(concurrentThreads)

	var runPerThread = totalRuns / concurrentThreads

	start := time.Now()
	for i := 0; i < concurrentThreads; i++ {
		go func() {
			defer wg.Done()
			for j := 0; j < runPerThread; j++ {
				workOn(db)
			}
		}()
	}

	wg.Wait()
	elapsed := time.Since(start)

	log.Println("Benchmark wall time", elapsed)
	rps := (float64(concurrentThreads) * float64(runPerThread))
	rps = rps / elapsed.Seconds()
	log.Println("Runs per seconds", rps)
}

func workOn(db *sql.DB) {
	db.Exec("SELECT 1")
	db.Exec("SELECT 1 + 3")
	db.Exec("SELECT 1 - 5")
	db.Exec("SELECT 1")
	db.Exec("SELECT 1 + 3")
	db.Exec("SELECT 1 - 5")
	db.Exec("SELECT 1")
	db.Exec("SELECT 1 + 3")
	db.Exec("SELECT 1 - 5")
	db.Exec("SELECT 1")
	db.Exec("SELECT 1 + 3")
	db.Exec("SELECT 1 - 5")
	db.Exec("SELECT 1")
	db.Exec("SELECT 1 + 3")
	db.Exec("SELECT 1 - 5")
	db.Exec("SELECT 1")
	db.Exec("SELECT 1 + 3")
	db.Exec("SELECT 1 - 5")
}
