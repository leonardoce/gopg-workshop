-- +goose Up
-- SQL in this section is executed when the migration is applied.
CREATE TABLE persone (
    id INTEGER PRIMARY KEY,
    nome VARCHAR(200),
    cognome VARCHAR(200)
);

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DROP TABLE persone;