-- +goose Up
-- SQL in this section is executed when the migration is applied.
CREATE SEQUENCE persone_id_seq;
ALTER TABLE persone ALTER id SET DEFAULT nextval('persone_id_seq');
ALTER SEQUENCE persone_id_seq OWNED BY persone.id;
SELECT SETVAL('persone_id_seq', COALESCE(MAX(id), 0)) FROM persone;

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
ALTER TABLE persone ALTER id DROP DEFAULT;
DROP SEQUENCE persone_id_seq;