package main

import (
	"database/sql"
	"log"

	_ "github.com/lib/pq"
)

func main() {
	db, err := sql.Open("postgres", "dbname=postgres user=postgres host=127.0.0.1 sslmode=disable")
	if err != nil {
		log.Fatal("sql.Open error", err)
	}

	err = db.Ping()
	if err != nil {
		log.Fatal("db ping failed", err)
	}

	_, err = db.Exec("SET ROLE TO chicca")
	if err != nil {
		log.Fatal("db set role failed", err)
	}

	var user string
	err = db.QueryRow("SELECT current_user::text").Scan(&user)
	if err != nil {
		log.Fatal("query failed", err)
	}

	log.Println(user)

	_, err = db.Exec("SET ROLE TO postgres")
	if err != nil {
		log.Fatal("db set role failed", err)
	}
}
