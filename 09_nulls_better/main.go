package main

import (
	"database/sql"
	"log"

	"gopkg.in/guregu/null.v3"

	_ "github.com/lib/pq"
)

func main() {
	db, err := sql.Open("postgres", "dbname=postgres user=postgres sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}

	var id int
	var nome null.String
	var cognome null.String
	rows, err := db.Query("SELECT id, nome, cognome FROM persone")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(&id, &nome, &cognome)
		if err != nil {
			log.Fatal(err)
		}
		log.Println(id, nome, cognome)
	}

	db.Close()
}
