package main

import (
	"encoding/json"
	"log"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	null "gopkg.in/guregu/null.v3"
)

// Persona rappresenta un record di una tabella
type Persona struct {
	ID      int         `db:"id" json:"id"`
	Nome    null.String `db:"nome" json:"nome"`
	Cognome null.String `db:"cognome" json:"cognome"`
}

func main() {
	db, err := sqlx.Connect("postgres", "dbname=postgres user=postgres sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}

	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}

	rows, err := db.Queryx("SELECT * FROM persone")
	if err != nil {
		log.Fatal(err)
	}

	for rows.Next() {
		var persona Persona
		err = rows.StructScan(&persona)
		if err != nil {
			log.Fatal(err)
		}

		log.Println(persona)
		result, _ := json.Marshal(persona)
		log.Println(string(result))
	}
}
