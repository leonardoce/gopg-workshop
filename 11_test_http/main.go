package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	null "gopkg.in/guregu/null.v3"
)

var db *sqlx.DB

// Persona rappresenta un record di una tabella
type Persona struct {
	ID      int         `db:"id" json:"id"`
	Nome    null.String `db:"nome" json:"nome"`
	Cognome null.String `db:"cognome" json:"cognome"`
}

func personeCountHandler(writer http.ResponseWriter, request *http.Request) {
	tx, err := db.Beginx()
	if err != nil {
		panic(err)
	}
	defer tx.Commit()

	var quantePersone int
	err = tx.Get(&quantePersone, "SELECT COUNT(*) FROM persone")
	if err != nil {
		panic(err)
	}

	writer.WriteHeader(200)
	writer.Write([]byte(strconv.Itoa(quantePersone)))
}

func personeListHandler(writer http.ResponseWriter, request *http.Request) {
	tx, err := db.Beginx()
	if err != nil {
		panic(err)
	}
	defer tx.Commit()

	rows, err := tx.Queryx("SELECT * FROM persone ORDER BY cognome, nome")
	if err != nil {
		panic(err)
	}

	var persone []Persona
	for rows.Next() {
		var persona Persona
		rows.StructScan(&persona)
		persone = append(persone, persona)
	}

	writer.WriteHeader(200)
	result, _ := json.Marshal(persone)
	writer.Write(result)
}

func main() {
	var err error
	db, err = sqlx.Open("postgres", "dbname=postgres user=postgres sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}

	http.HandleFunc("/persone/count", personeCountHandler)
	http.HandleFunc("/persone/list", personeListHandler)
	http.ListenAndServe(":9000", nil)
}
