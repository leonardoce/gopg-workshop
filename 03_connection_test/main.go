package main

import (
	"database/sql"
	"log"

	_ "github.com/lib/pq"
)

func main() {
	db, err := sql.Open("postgres", "dbname=aoeuaoeuaoeuaoeu user=postgres host=127.0.0.1 sslmode=disable")
	if err != nil {
		log.Fatal("sql.Open error", err)
	}

	db.Close()
}
